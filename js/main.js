"use strict";

const permission = document.querySelector(".permission");
const positive = document.querySelector(".positive");
const negative = document.querySelector(".negative");
const error = document.querySelector(".error");

const API_KEY = "8d242b2c68a6ed2b5a39a69d963c7d39";
const RAIN_MARGIN = 4;

// Función que hace una petición a un URL y devuelve el JSON
async function getData({ url, options = {} }) {
  const response = await fetch(url, options);

  if (!response.ok) {
    throw new Error("Error en la petición");
  }

  const data = await response.json();

  return data;
}

// Función que muestra un panel
function showPanel(panel) {
  hideAllPanels();
  panel.classList.remove("hidden");
}

// Función que oculta todos los paneles
function hideAllPanels() {
  permission.classList.add("hidden");
  positive.classList.add("hidden");
  negative.classList.add("hidden");
  error.classList.add("hidden");
}

// Función que se ejecutará si va a llover
function showPositive({ city, temperature, weather, nextRain }) {
  showPanel(positive);
  const text = positive.querySelector("p");
  text.innerHTML = `
    Ahora mismo hay ${temperature}ºC en <strong>${city}</strong> con <strong>${weather}</strong> y 
    ${
      nextRain > 0
        ? `probablemente llueva en <strong>${nextRain} hora(s)</strong>`
        : "está lloviendo <strong>ahora mismo</strong>"
    }
  `;
}

// Función que se ejecutará si no va a llover
function showNegative({ city, weather, temperature }) {
  showPanel(negative);
  const text = negative.querySelector("p");
  text.innerHTML = `
  Ahora mismo hay ${temperature}ºC en <strong>${city}</strong> con <strong>${weather}</strong> y parece que <strong>no lloverá</strong> en las próximas ${RAIN_MARGIN} hora(s).
  `;
}

// Función que pide la información del tiempo y detecta si lloverá o no
async function getWeatherData({ latitude, longitude }) {
  try {
    // Pedir estado actual a la API
    const currentWeather = await getData({
      url: `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${API_KEY}&units=metric&lang=es`,
    });

    // Pedir predicción próximas horas a la API
    const nextHours = await getData({
      url: `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=current,minutely,daily&appid=${API_KEY}&units=metric&lang=es`,
    });

    // Comprobar si va a llover en las próximas RAIN_MARGIN horas
    const nextRain = nextHours.hourly.findIndex((hour) => {
      return hour.weather[0].main === "Rain";
    });

    // Si llueve mostrar panel positive
    // Si no llueve mostrar panel negative

    if (nextRain > -1 && nextRain <= RAIN_MARGIN) {
      showPositive({
        city: currentWeather.name,
        temperature: currentWeather.main.temp,
        weather: currentWeather.weather[0].description,
        nextRain,
      });
    } else {
      showNegative({
        city: currentWeather.name,
        temperature: currentWeather.main.temp,
        weather: currentWeather.weather[0].description,
      });
    }
  } catch (error) {
    // Si hay un error mostrar error
    showPanel(error);
  }
}

// Función que pide la localización al usuario
function getUserLocation() {
  hideAllPanels();
  navigator.geolocation.getCurrentPosition(
    (location) => {
      const { latitude, longitude } = location.coords;

      getWeatherData({ latitude, longitude });
      localStorage.setItem("permission", "ok");
    },
    () => {
      showPanel(error);
    }
  );
}

// Función principal
function main() {
  if (localStorage.getItem("permission") === "ok") {
    getUserLocation();
  } else {
    showPanel(permission);

    const permissionButton = permission.querySelector("button");

    permissionButton.onclick = () => {
      getUserLocation();
    };
  }
}

main();
